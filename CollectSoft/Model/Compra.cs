﻿using System;

namespace CollectSoft.Model
{
    public class Compra
    {
        public int Id { get; set; }
        public Cliente Cliente { get; set; }
        public string Articulo { get; set; }
        public decimal Cantidad { get; set; }
        public DateTime CompradoEn { get; set; }
    }
}
