﻿using CollectSoft.Model;
using System;
using System.IO.Ports;
using ThermalDotNet;

namespace CollectSoft.Helpers
{
    public static class Impresion
    {
        public static bool Imprimir(this Pago pago)
        {
            try
            {
                var port = Properties.Settings.Default.Port;
                var serialPort = new SerialPort(port, 9600);
                if (serialPort.IsOpen)
                {
                    serialPort.Close();
                }

                serialPort.Open();
                var printer = new ThermalPrinter(serialPort, 2, 180, 2);
                printer.WakeUp();
                TextoImpresion(printer, pago);
                printer.Sleep();
                serialPort.Close();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private static void TextoImpresion(ThermalPrinter printer, Pago pago)
        {
            var barcodeType = ThermalPrinter.BarcodeType.ean13;
            printer.SetLineSpacing(0);
            printer.SetAlignCenter();
            printer.WriteLine_Bold("REGALOS Y NOVEDADES ARMANI");
            printer.SetAlignCenter();
            printer.WriteLine_Bold("ABONO A CREDITO");
            printer.SetAlignLeft();
            printer.WriteLine($"PAGO #: {pago.Id}");
            printer.LineFeed();
            printer.WriteLine($"Cliente: {pago.Cliente.Nombre}");
            printer.WriteLine($"Fecha: {pago.PagadoEn}");
            printer.LineFeed();
            printer.LineFeed();
            printer.WriteLine_Bold("DATOS DEL PAGO");
            printer.LineFeed();
            printer.WriteLine($"SALDO ANTERIOR: {pago.Pagado + pago.Cliente.Adeudado}");
            printer.WriteLine($"PAGADO: {pago.Pagado}");
            printer.WriteLine($"PENDIENTE: {pago.Cliente.Adeudado}");
            printer.LineFeed();
            printer.LineFeed();
            printer.WriteLine_Bold("________________________");
            printer.WriteLine_Bold("Firma");
            printer.LineFeed();
            printer.LineFeed();
            printer.SetLargeBarcode(false);
            printer.PrintBarcode(barcodeType, pago.Id.ToString().PadLeft(6, '0'));
            printer.LineFeed();
            printer.LineFeed();
            printer.LineFeed();
        }
    }
}
