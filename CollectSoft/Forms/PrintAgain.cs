﻿using CollectSoft.Helpers;
using CollectSoft.Model;
using LiteDB;
using System;
using System.ComponentModel;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace CollectSoft.Forms
{
    public partial class PrintAgain : Form
    {
        public PrintAgain()
        {
            InitializeComponent();
        }

        private void textBox_Validating(object sender, CancelEventArgs e)
        {
            var regex = new Regex("^[0-9]+$");
            if (!regex.IsMatch(textBox.Text))
            {
                MessageBox.Show("Este campo solo permite números", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBox.Text = string.Empty;
                e.Cancel = true;
            }
        }

        private void button_Click(object sender, EventArgs e)
        {
            var connection = ConfigurationManager.ConnectionStrings["LiteDB"].ConnectionString;
            using (var db = new LiteDatabase(connection))
            {
                var pagos = db.GetCollection<Pago>("pagos");
                var clientes = db.GetCollection<Cliente>("clientes");
                var pago = pagos.FindById(Convert.ToInt32(textBox.Text));
                pago.Cliente = clientes.FindById(pago.Cliente.Id);
                if (!pago.Imprimir())
                {
                    MessageBox.Show("Error al imprimir", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                Close();
            }
        }
    }
}
