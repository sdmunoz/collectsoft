﻿using CollectSoft.Model;
using LiteDB;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace CollectSoft
{
    public partial class ClientSell : Form
    {
        public ClientSell(Cliente obj)
        {
            InitializeComponent();

            clienteBindingSource.DataSource = obj;
            compraBindingSource.DataSource = new Compra();
        }

        private void btnGuardar_Click(object sender, System.EventArgs e)
        {
            try
            {
                var connection = ConfigurationManager.ConnectionStrings["LiteDB"].ConnectionString;
                using (var db = new LiteDatabase(connection))
                {
                    var compras = db.GetCollection<Compra>("compras");
                    var clientes = db.GetCollection<Cliente>("clientes");
                    var compra = compraBindingSource.Current as Compra;
                    var cliente = clienteBindingSource.Current as Cliente;
                    cliente.Adeudado += compra.Cantidad;
                    compra.Cliente = cliente; 
                    compra.CompradoEn = dtpFecha.Value.Date;
                    compras.Insert(compra);
                    clientes.Update(cliente);
                    MessageBox.Show("Creado correctamente", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Close();
                }
            }
            catch
            {
                MessageBox.Show("Error al insertar la venta", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtCantidad_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var regex = new Regex("^[0-9]+$");
            if (!regex.IsMatch(txtCantidad.Text))
            {
                MessageBox.Show("Este campo solo permite números", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtCantidad.Text = string.Empty;
                e.Cancel = true;
            }
        }
    }
}
