﻿using CollectSoft.Model;
using LiteDB;
using System.Configuration;
using System.Linq;
using System.Windows.Forms;

namespace CollectSoft
{
    public partial class ClientList : Form
    {
        public ClientList()
        {
            InitializeComponent();
            LoadListBox();
        }

        private void LoadListBox()
        {
            var connection = ConfigurationManager.ConnectionStrings["LiteDB"].ConnectionString;
            using (var db = new LiteDatabase(connection))
            {
                var clientes = db.GetCollection<Cliente>("clientes");
                clienteBindingSource.DataSource = clientes.FindAll();
            }
        }

        private void Search(string text)
        {
            if (!string.IsNullOrWhiteSpace(text))
            {
                var connection = ConfigurationManager.ConnectionStrings["LiteDB"].ConnectionString;
                using (var db = new LiteDatabase(connection))
                {
                    var clientes = db.GetCollection<Cliente>("clientes");
                    clientes.EnsureIndex(x => x.Nombre);
                    clienteBindingSource.DataSource = clientes.Find(x => x.Nombre.ToLower().StartsWith(text)).ToList();
                } 
            }
            else
            {
                LoadListBox();
            }
        }

        private void listBox_DoubleClick(object sender, System.EventArgs e)
        {
            var obj = clienteBindingSource.Current as Cliente;
            new ClientDashboard(obj).ShowDialog();
        }

        private void txtNombre_TextChanged(object sender, System.EventArgs e)
        {
            Search(txtNombre.Text);
        }
    }
}
