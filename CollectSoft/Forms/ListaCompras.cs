﻿using CollectSoft.Model;
using LiteDB;
using System.Configuration;
using System.Linq;
using System.Windows.Forms;

namespace CollectSoft
{
    public partial class ListaCompras : Form
    {
        public ListaCompras(Cliente obj)
        {
            InitializeComponent();

            CargarGrid(obj);
        }

        private void CargarGrid(Cliente obj)
        {
            var connection = ConfigurationManager.ConnectionStrings["LiteDB"].ConnectionString;
            using (var db = new LiteDatabase(connection))
            {
                var compras = db.GetCollection<Compra>("compras");
                compraBindingSource.DataSource = compras.Include(x => x.Cliente).Find(x => x.Cliente.Id == obj.Id).ToList();
            }
        }

        private void dataGridView_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView.CurrentRow == null) return;
            var obj = compraBindingSource.Current as Compra;
            if (MessageBox.Show("¿Desea borrar la compra?", "Mensaje", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                try
                {
                    var connection = ConfigurationManager.ConnectionStrings["LiteDB"].ConnectionString;
                    using (var db = new LiteDatabase(connection))
                    {
                        var compras = db.GetCollection<Compra>("compras");
                        var clientes = db.GetCollection<Cliente>("clientes");
                        var cliente = clientes.FindById(obj.Cliente.Id);
                        cliente.Adeudado -= obj.Cantidad;
                        clientes.Update(cliente);
                        compras.Delete(Query.EQ("_id", obj.Id));
                        Close();
                    }
                }
                catch
                {
                    MessageBox.Show("Error al borrar la compra", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
