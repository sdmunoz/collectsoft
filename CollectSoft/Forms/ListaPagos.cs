﻿using CollectSoft.Model;
using LiteDB;
using System.Configuration;
using System.Linq;
using System.Windows.Forms;

namespace CollectSoft
{
    public partial class ListaPagos : Form
    {
        public ListaPagos(Cliente obj)
        {
            InitializeComponent();
            CargarGrid(obj);
        }

        private void CargarGrid(Cliente obj)
        {
            var connection = ConfigurationManager.ConnectionStrings["LiteDB"].ConnectionString;
            using (var db = new LiteDatabase(connection))
            {
                var pagos = db.GetCollection<Pago>("pagos");
                pagoBindingSource.DataSource = pagos.Include(x => x.Cliente).Find(x => x.Cliente.Id == obj.Id).ToList();
            }
        }

        private void dataGridView_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView.CurrentRow == null) return;
            var obj = pagoBindingSource.Current as Pago;
            if (MessageBox.Show("¿Desea borrar el pago realizado?", "Mensaje", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                try
                {
                    var connection = ConfigurationManager.ConnectionStrings["LiteDB"].ConnectionString;
                    using (var db = new LiteDatabase(connection))
                    {
                        var pagos = db.GetCollection<Pago>("pagos");
                        var clientes = db.GetCollection<Cliente>("clientes");
                        var cliente = clientes.FindById(obj.Cliente.Id);
                        cliente.Adeudado += obj.Pagado;
                        clientes.Update(cliente);
                        pagos.Delete(Query.EQ("_id", obj.Id));
                        Close();
                    }
                }
                catch
                {
                    MessageBox.Show("Error al borrar el pago", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
