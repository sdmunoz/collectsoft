﻿using System;
using System.Windows.Forms;

namespace CollectSoft
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            CenterControl(welcomeLabel);
            CenterControl(subLabel);
        }

        private void CenterControl(Control ctl)
        {
            ctl.Left = (ClientSize.Width - ctl.Width) / 2;
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            CenterControl(welcomeLabel);
            CenterControl(subLabel);
        }

        private void btnAddClient_Click(object sender, EventArgs e)
        {
            new CreateClientForm(null).ShowDialog();
        }

        private void btnListado_Click(object sender, EventArgs e)
        {
            new ClientList().ShowDialog();
        }

        private void btnConfiguracion_Click(object sender, EventArgs e)
        {
            new Forms.Configuration().ShowDialog();
        }

        private void btnReimpresion_Click(object sender, EventArgs e)
        {
            new Forms.PrintAgain().ShowDialog();
        }
    }
}
