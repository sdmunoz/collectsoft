﻿using CollectSoft.Model;
using LiteDB;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace CollectSoft
{
    public partial class CreateClientForm : Form
    {
        bool isEdit = false;

        public CreateClientForm(Cliente obj)
        {
            InitializeComponent();

            if (obj == null)
            {
                clienteBindingSource.DataSource = new Cliente(); 
                isEdit = false;
            }
            else
            {
                clienteBindingSource.DataSource = obj;
                isEdit = true;
            }
        }

        private void btnAgregar_Click(object sender, System.EventArgs e)
        {
            try
            {
                var connection = ConfigurationManager.ConnectionStrings["LiteDB"].ConnectionString;
                using (var db = new LiteDatabase(connection))
                {
                    var clientes = db.GetCollection<Cliente>("clientes");
                    if (!isEdit)
                    {
                        clientes.Insert(clienteBindingSource.Current as Cliente);
                        MessageBox.Show("Creado correctamente", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information); 
                    }
                    else
                    {
                        clientes.Update(clienteBindingSource.Current as Cliente);
                        MessageBox.Show("Cliente actualizado correctamente", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information); 
                    }
                    Close();
                }
            }
            catch
            {
                MessageBox.Show("Error al agregar el cliente", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtLimite_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var regex = new Regex("^[0-9]+$");
            if (!regex.IsMatch(txtLimite.Text))
            {
                MessageBox.Show("Este campo solo permite números", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtLimite.Text = string.Empty;
                e.Cancel = true;
            }
        }
    }
}
