﻿using CollectSoft.Model;
using LiteDB;
using System.Configuration;
using System.Windows.Forms;

namespace CollectSoft
{
    public partial class ClientDashboard : Form
    {
        public ClientDashboard(Cliente obj)
        {
            InitializeComponent();

            clienteBindingSource.DataSource = obj;
        }

        private void btnAddVenta_Click(object sender, System.EventArgs e)
        {
            var cliente = clienteBindingSource.Current as Cliente;
            new ClientSell(cliente).ShowDialog();
            UpdateData(cliente.Id);
        }

        private void UpdateData(int id)
        {
            var connection = ConfigurationManager.ConnectionStrings["LiteDB"].ConnectionString;
            using (var db = new LiteDatabase(connection))
            {
                var clientes = db.GetCollection<Cliente>("clientes");
                clienteBindingSource.DataSource = clientes.FindById(id);
            }
        }

        private void btnEditarCliente_Click(object sender, System.EventArgs e)
        {
            var cliente = clienteBindingSource.Current as Cliente;
            new CreateClientForm(cliente).ShowDialog();
            UpdateData(cliente.Id);
        }

        private void btnCobrar_Click(object sender, System.EventArgs e)
        {
            var cliente = clienteBindingSource.Current as Cliente;
            new ClientPay(cliente).ShowDialog();
            UpdateData(cliente.Id);
        }

        private void btnVentasCliente_Click(object sender, System.EventArgs e)
        {
            var cliente = clienteBindingSource.Current as Cliente;
            new ListaCompras(cliente).ShowDialog();
            UpdateData(cliente.Id);
        }

        private void btnPagoCliente_Click(object sender, System.EventArgs e)
        {
            var cliente = clienteBindingSource.Current as Cliente;
            new ListaPagos(cliente).ShowDialog();
            UpdateData(cliente.Id);
        }
    }
}
