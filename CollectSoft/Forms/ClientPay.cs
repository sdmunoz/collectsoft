﻿using CollectSoft.Helpers;
using CollectSoft.Model;
using LiteDB;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace CollectSoft
{
    public partial class ClientPay : Form
    {
        public ClientPay(Cliente obj)
        {
            InitializeComponent();

            clienteBindingSource.DataSource = obj;
            pagoBindingSource.DataSource = new Pago();
        }

        private void btnPay_Click(object sender, System.EventArgs e)
        {
            Save(false);
        }

        private void Save(bool print = true)
        {
            try
            {
                var connection = ConfigurationManager.ConnectionStrings["LiteDB"].ConnectionString;
                using (var db = new LiteDatabase(connection))
                {
                    var pagos = db.GetCollection<Pago>("pagos");
                    var clientes = db.GetCollection<Cliente>("clientes");
                    var pago = pagoBindingSource.Current as Pago;
                    var cliente = clienteBindingSource.Current as Cliente;
                    cliente.Adeudado -= pago.Pagado;
                    pago.Cliente = cliente;
                    pago.Pendiente = cliente.Adeudado;
                    pago.PagadoEn = dtpFecha.Value.Date;
                    pagos.Insert(pago);
                    clientes.Update(cliente);

                    if (!print)
                    {
                        MessageBox.Show("Abono guardado correctamente", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }

                    if (!pago.Imprimir())
                    {
                        MessageBox.Show("Error al imprimir", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch
            {
                MessageBox.Show("Error al insertar el pago", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnPayAndPrint_Click(object sender, System.EventArgs e)
        {
            Save(true);
        }

        private void txtCantidad_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var regex = new Regex("^[0-9]+$");
            if (!regex.IsMatch(txtCantidad.Text))
            {
                MessageBox.Show("Este campo solo permite números", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtCantidad.Text = string.Empty;
                e.Cancel = true;
            }
        }
    }
}
