﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DatabaseMigration.Models
{
    [Table("compras")]
    public class Compra
    {
        [Column("_id")]
        public int Id { get; set; }
        public int ClienteId { get; set; }
        public string Articulo { get; set; }
        public decimal Cantidad { get; set; }
        [Column("Fecha")]
        public string Fecha { get; set; }
    }
}
