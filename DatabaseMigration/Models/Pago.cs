﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DatabaseMigration.Models
{
    [Table("pagos")]
    public class Pago
    {
        [Column("_id")]
        public int Id { get; set; }
        public int ClienteID { get; set; }
        public decimal Pagado { get; set; }
        public decimal Pendiente { get; set; }
        [Column("Fecha")]
        public string Fecha { get; set; }
    }
}
