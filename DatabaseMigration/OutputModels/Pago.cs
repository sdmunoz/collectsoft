﻿using System;

namespace DatabaseMigration.OutputModels
{
    public class Pago
    {
        public int Id { get; set; }
        public Cliente Cliente { get; set; }
        public decimal Pagado { get; set; }
        public decimal Pendiente { get; set; }
        public DateTime PagadoEn { get; set; }
    }
}
