﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseMigration.OutputModels
{
    public class Compra
    {
        public int Id { get; set; }
        public Cliente Cliente { get; set; }
        public string Articulo { get; set; }
        public decimal Cantidad { get; set; }
        public DateTime CompradoEn { get; set; }
    }
}
