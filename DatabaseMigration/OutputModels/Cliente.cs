﻿namespace DatabaseMigration.OutputModels
{
    public class Cliente
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public decimal Limite { get; set; }
        public decimal Adeudado { get; set; }
    }
}
