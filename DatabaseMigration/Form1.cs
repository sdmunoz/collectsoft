﻿using Dapper;
using DatabaseMigration.Models;
using LiteDB;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Windows.Forms;

namespace DatabaseMigration
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void txtFile_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            var ofd = new OpenFileDialog
            {
                FileName = "",
                Filter = "SQLite 3 database (*.db)|*.db"  
            };
            if(ofd.ShowDialog() == DialogResult.OK)
            {
                txtFile.Text = ofd.FileName;
            }
        }

        private void button_Click(object sender, System.EventArgs e)
        {
            var dataCliente = Generate<Cliente>("clientes");
            var dataCompra = Generate<Compra>("compras");
            var dataPago = Generate<Pago>("pagos");
            DumpInDatabaseCliente(dataCliente);
            DumpInDatabaseCompra(dataCompra);
            DumpInDatabasePago(dataPago);
        }

        private IEnumerable<T> Generate<T>(string table)
        {
            using (var connection = new SQLiteConnection($"Data Source={txtFile.Text};Version=3"))
            {
                string query = $"select * from {table}";
                connection.Open();
                return connection.Query<T>(query).ToList();
            }
        }

        private void DumpInDatabaseCliente(IEnumerable<Cliente> data)
        {
            IEnumerable<OutputModels.Cliente> cliente = data.Select(x => new OutputModels.Cliente { Id = x.Id, Nombre = x.Nombre, Telefono = x.Telefono, Direccion = x.Direccion, Adeudado = x.Adeudado, Limite = x.Limite });
            if (cliente != null)
            {
                using (var db = new LiteDatabase("Filename=collectDatabase.db"))
                {
                    var collection = db.GetCollection<OutputModels.Cliente>("clientes");
                    collection.InsertBulk(cliente);
                    txtResult.AppendText("Tabla cliente creada.\r\n");
                }
            }
        }

        private void DumpInDatabaseCompra(IEnumerable<Compra> data)
        {
            IEnumerable<OutputModels.Compra> compra = data.Select(x => new OutputModels.Compra { Id = x.Id, Articulo = x.Articulo, Cliente = new OutputModels.Cliente { Id = x.ClienteId }, Cantidad = x.Cantidad, CompradoEn = DateTime.Parse(x.Fecha) });
            if (compra != null)
            {
                using (var db = new LiteDatabase("Filename=collectDatabase.db"))
                {
                    var collection = db.GetCollection<OutputModels.Compra>("compras");
                    collection.InsertBulk(compra);
                    txtResult.AppendText("Tabla compra creada.\r\n");
                }
            }
        }

        private void DumpInDatabasePago(IEnumerable<Pago> data)
        {
            IEnumerable<OutputModels.Pago> pago = data.Select(x => new OutputModels.Pago { Id = x.Id, Cliente = new OutputModels.Cliente { Id = x.ClienteID }, PagadoEn = DateTime.Parse(x.Fecha), Pagado = x.Pagado, Pendiente = x.Pendiente });
            if (pago != null)
            {
                using (var db = new LiteDatabase("Filename=collectDatabase.db"))
                {
                    var collection = db.GetCollection<OutputModels.Pago>("pagos");
                    collection.InsertBulk(pago);
                    txtResult.AppendText("Tabla pago creada.\r\n");
                }
            }
        }
    }
}
